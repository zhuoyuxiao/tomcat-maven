package com.zyx.tomcat.tmp;

import com.zyx.tomcat.TomcatStartTest;
import com.zyx.tomcat.util.WarUtil;

public class MyBuildUtil {

	private static final String GRADLE_SITE_DIR ="D:\\gradleProject";
	
	public static void main(String[] args) throws Exception {
//		startGradleSite("haiwai", 8089);
//		System.out.println(System.getProperty("os.name"));
		TomcatStartTest.startTomcat("pointSystem-web", 80, "D:\\webutil\\apache-tomcat-7.0.39\\webapps\\pointSystem-web");
	}
	
	private static void startGradleSite(String site,Integer port) throws Exception {
		Process process=Runtime.getRuntime().exec("cmd /c cd "+GRADLE_SITE_DIR+"\\"+site+" & "+"gradle clean & gradle build");
		process.waitFor();
		unZipWar(site);
		TomcatStartTest.startTomcat(site, port, getWebappPath(site));
	}
	
	private static void unZipWar(String site) {
		WarUtil.unzip(getWarPath(site), getWebappPath(site));
	}
	
	private static String getWebappPath(String site) {
		return GRADLE_SITE_DIR+"\\"+site+"\\src\\main\\webapp";
	}
	
	private static String getWarPath(String site) {
		return GRADLE_SITE_DIR+"\\"+site+"\\build\\libs\\"+site+".war";
	}
}
