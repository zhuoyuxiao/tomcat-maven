package com.zyx.tomcat.tmp;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.Node;

import com.zyx.tomcat.TomcatStartTest;
import com.zyx.tomcat.util.FileUtil;
import com.zyx.tomcat.util.MyTomcatUtil;
import com.zyx.tomcat.util.XmlUtil;
@SuppressWarnings("unused")
public class PjaBuildUtil {
    private static final String BUILD_DIR ="D:\\project\\projectA\\build";
    private static final String GRADLE_SITE_DIR ="D:\\gradleProject";
    private static final File SOURCE_DIR = new File("D:\\project\\projectA\\source");
    private static final File SHARE_JARS_DIR = new File("D:\\project\\infra\\source\\jars");
    ///上面根据个人情况而修改
    
    private static final File PJA_JARS_DIR = new File(SOURCE_DIR+"\\pja_common\\lib");
    private static final String LIB_DIR = "\\WEB-INF\\lib";
    private static final String CLASS_DIR = "\\WEB-INF\\classes";
    private static final File ALL_JARS_DIR = new File("E:\\tmp\\pjajars");
    private static final Set<String> SHARE_JARS_WHITE_LIST = new HashSet<>(Arrays.asList("loginclient.jar", "logindata.jar"
            , "baitian-common.jar", "baitian-monitor.jar", "qqlogin.jar", "pja-api.jar", "ipservice.jar" , "poi-3.7-20101029.jar" , "lucene-core-3.4.0.jar" 
            , "lucene-misc-3.4.0.jar" , "junit.jar" , "pinyin4j-2.5.0.jar", "poi-3.7-20101029.jar", "jxl.jar")) ;
   
	public static void main(String[] args) throws Exception {
//    	String site="cms";
//        initSite(site);
//        buildCommon();
//        setClassByOldClassPath("pja_common");
//        startSite(site,8081,false);
//        copyAllJarsIfNeed();
//        setClassPath(site);
//        Scanner scanner = new Scanner(System.in);
//        System.out.print("选择服务器: ");
//        int num = scanner.nextInt();
//        if(num==1) {
//        	startSite("cms", 8081, false);
//        }else if(num==6){
//        	startSite("bgm", 8086, false);
//        }else if(num==9){
//        	startSite("haiwai", 8089, true);
//        }
        startSite("haiwai", 8089, true);
    }

	private static void startSite(String site,Integer port,boolean isGradle) throws Exception {
		if(isGradle) {
			ProcessResourceFileHelper.process(getBin(site,isGradle)+"/profile/dev.properties", getBin(site,isGradle), Pattern.compile(".*\\.(properties|xml)"));
		}
		copyBin2Classes(site,isGradle);
		TomcatStartTest.startTomcat(site, port, getSiteWebRoot(site,isGradle));
	}

	private static void copyBin2Classes(String site, boolean isGradle) throws IOException {
		if(!isGradle) {
			return;
		}
		FileUtils.copyDirectory(new File(getBin(site,isGradle)), new File(getClassesDir(site,isGradle)));
	}

	private static String getClassesDir(String site, boolean isGradle) {
		return getSiteWebRoot(site, isGradle)+"\\WEB-INF\\classes";
	}

	private static String getBin(String site, boolean isGradle) {
		return getSiteRoot(site,isGradle)+"\\bin";
	}
	private static void initSite(String site) throws Exception, IOException {
		build(site);
        setClassPath(site);
	}
	private static void buildCommon() throws Exception {
        callbuildSite("pja_web_common pja_common");
        FileUtil.copyDirectory(BUILD_DIR+"\\pja_common_jars", ALL_JARS_DIR.getAbsolutePath());
    }
    private static void build(String site) throws Exception {
        copyAllJarsIfNeed();
        callbuildSite(site);
        String buildSiteDir = String.format("%s\\%s\\%s",BUILD_DIR,site,site);
        String siteDir = getSiteWebRoot(site,false);
        for (String copyDir : new String[] {CLASS_DIR,LIB_DIR}) {
            FileUtil.delete(siteDir+copyDir);
            FileUtil.copyDirectory(buildSiteDir+copyDir,siteDir+copyDir);
        }
        
    }
    private static void callbuildSite(String site) throws Exception {
        MyTomcatUtil.runCmds("cd /D "+BUILD_DIR,"call ant "+site);
    }
    private static void setClassByOldClassPath(String site) throws Exception {
    	List<String> jars = getJarsByClassPath(site);
    	setClassPathByJars(site, jars);
		
	}
	private static List<String> getJarsByClassPath(String site) throws Exception {
		String oldClassPath = getClassPath(site);
    	List<Node> tags = XmlUtil.getByTagName(XmlUtil.getDoc(oldClassPath),"classpathentry");
    	List<String> jars=new ArrayList<>();
    	for (Node node : tags) {
			String path = XmlUtil.getAttr(node, "path");
			if(path==null || !path.endsWith(".jar")) {
				continue;
			}
			int index = path.lastIndexOf('/');
			index=index==-1?path.lastIndexOf('\\'):index;
			jars.add(path.substring(index+1));
		}
		return jars;
	}
    private static void setClassPath(String site) throws Exception {

    	ArrayList<String> jars =new ArrayList<>(Arrays.asList(new File(getSiteWebRoot(site,false)+LIB_DIR).list()));
    	//pja_web_common,pja_common依赖
    	jars.addAll(getJarsByClassPath("pja_web_common"));
    	jars.addAll(getJarsByClassPath("pja_common"));
        //pja_web_common,pja_common依赖
        setClassPathByJars(site, jars);
        
    }
    private static void setClassPathByJars(String site,List<String> jars) throws Exception {
    	jars=jars.stream().sorted().distinct().collect(Collectors.toList());
    	Map<String, String> jar2SourcePath=new HashMap<>();
    	jar2SourcePath.put("pja_web_common.jar", SOURCE_DIR+"\\pja_web_common\\src");
    	jar2SourcePath.put("pja_common.jar", SOURCE_DIR+"\\pja_common\\src");
    	jars.remove("pja_web_common.jar");
    	jars.remove("pja_common.jar");
    	String classPathBase=
    			"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
    					"<classpath>\n" + 
    					"    <classpathentry kind=\"src\" path=\"src\"/>\n" + 
    					"	 <classpathentry kind=\"src\" path=\"pjaWebCommonSrc\"/>\n"+
    					"	 <classpathentry kind=\"src\" path=\"pjaCommonSrc\"/>\n"+
    					"    <classpathentry kind=\"con\" path=\"org.eclipse.jdt.launching.JRE_CONTAINER\"/>\n" + 
    					"    <classpathentry kind=\"output\" path=\"WebRoot/WEB-INF/classes\"/>\n%s" + 
    					"\n</classpath>";
    	StringBuilder libDependences=new StringBuilder();
    	for (String jarName: jars) {
    		String sourcePath = jar2SourcePath.get(jarName);
    		libDependences.append(String.format("\n\t<classpathentry kind=\"lib\" path=\""+ALL_JARS_DIR.getAbsolutePath()+"\\%s\" %s/>"
    				, jarName,sourcePath!=null?new StringBuilder("sourcepath=\"").append(sourcePath).append("\""):""));
    	}
    	FileUtils.write(new File(getClassPath(site)), String.format(classPathBase, libDependences));
    	String project = IOUtils.toString(PjaBuildUtil.class.getResource("template.project"));
    	project=project.replace("${projectSiteName}", site);
    	project=project.replace("${pjaSource}", SOURCE_DIR.getAbsolutePath().replace('\\', '/'));
    	FileUtils.write(new File(getSiteRoot(site)+".project"),project,StandardCharsets.UTF_8);

    }
	private static String getClassPath(String site) {
		return getSiteRoot(site)+".classpath";
	}
    private static String getSiteWebRoot(String site, boolean isGradle) {
    	if(isGradle){
    		return GRADLE_SITE_DIR+"\\"+site+"\\src\\main\\webapp";
    	}
        return getSiteRoot(site)+"\\WebRoot\\";
    }
	private static String getSiteRoot(String site, boolean isGradle) {
		if(isGradle){
    		return GRADLE_SITE_DIR+"\\"+site;
    	}
		 return SOURCE_DIR+"\\"+site+"\\";
	}

    private static String getSiteRoot(String site) {
        return getSiteRoot(site, false);
    }
    private static void copyAllJarsIfNeed() throws IOException {
        if(ALL_JARS_DIR.exists()) {
            return;
        }
        ALL_JARS_DIR.mkdirs();
        FileUtil.copyAllFile(PJA_JARS_DIR,ALL_JARS_DIR,null);
        FileUtil.copyAllFile(SHARE_JARS_DIR,ALL_JARS_DIR,SHARE_JARS_WHITE_LIST);
    }
    
    

}
