package com.zyx.tomcat.tmp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.zyx.tomcat.util.FileUtil;

public class ProcessResourceFileHelper {
    public static void process(String profilePropertiesPath,String classDir, Pattern processResourcePattern) throws Exception {
        Pattern replaceHolder = Pattern.compile("\\$\\{(.*?)\\}");
        Properties profile = new Properties();
        profile.load(new FileInputStream(profilePropertiesPath));
        processResourceFile(profile, replaceHolder, new File(classDir),processResourcePattern);
    }

    private static void processResourceFile(Properties profile, Pattern replaceHolder, File file,Pattern processResourcePattern) throws IOException {
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                processResourceFile(profile, replaceHolder, f,processResourcePattern);
            }
        } else {
            if (processResourcePattern.matcher(file.getName()).find()) {
                String fileContent = FileUtil.readToString(file.getAbsolutePath());
                Matcher match = replaceHolder.matcher(fileContent);
                StringBuffer sb = new StringBuffer();
                boolean withMatch = false;
                while (match.find()) {
                    withMatch = true;
                    String var="";
                    try {
                        var = match.group(1);
                        match.appendReplacement(sb, profile.getProperty(var));
                    } catch (Exception e) {
                       throw new RuntimeException("file="+file+",var="+var, e);
                    }

                }
                if (withMatch) {
                    match.appendTail(sb);
                    FileUtil.writeFile(file.getAbsolutePath(), sb.toString());
                }
            }
        }
    }
}
