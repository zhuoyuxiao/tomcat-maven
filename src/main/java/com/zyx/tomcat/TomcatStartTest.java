package com.zyx.tomcat;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.catalina.startup.Bootstrap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.zyx.tomcat.tmp.PjaBuildUtil;
import com.zyx.tomcat.util.BuildUtil;
import com.zyx.tomcat.util.TomcatELUtil;
import com.zyx.tomcat.util.XmlUtil;
public class TomcatStartTest {
	
	
	
	public static void startTomcat(String site, int httpPort,String webRootDir) throws Exception {
		File siteXml = getTomcatSiteConfXml(site);
		String templateStr = IOUtils.toString(PjaBuildUtil.class.getResourceAsStream("/template-server.xml"),StandardCharsets.UTF_8);
		Map<String, Object> context=new HashMap<>();
		context.put("stopPort", httpPort+1000);
		context.put("httpPort", httpPort);
		context.put("webRootDir",webRootDir);
		String xml = TomcatELUtil.eval(context, templateStr).toString();
		FileUtils.write(siteXml, xml,StandardCharsets.UTF_8);
		TomcatStartTest.start(getTomcatHome(),site);
	}

	private static String getTomcatHome() {
		return System.getProperty("user.dir")+"\\tomcatHome\\";
	}
	private static File getTomcatSiteConfXml(String site) {
		File file = new File(getTomcatHome()+"conf\\site\\"+site+"-server.xml");
		file.getParentFile().mkdirs();
		return file;
	}
	private static void start(String tomcatHome,String site) throws Exception {
		String configFile = tomcatHome + "/conf/site/" + site + "-server.xml";
		buildBeforeRun(configFile);
		System.setProperty("catalina.home", tomcatHome);
		Bootstrap.main(new String[] { "-config", configFile, "start" });
	}
	public static void buildBeforeRun(String configFile) throws Exception {
		Document confServerXml = XmlUtil.getDoc(configFile);
		System.out.println(configFile);

		List<Node> tags = XmlUtil.getByTagName(confServerXml, "Context");
		for (Node tag : tags) {
			String hysBuild =XmlUtil.getAttr(tag, "hysBuild");
			if(!Objects.equals(hysBuild, "true")){
				continue;
			}
			String docBase = XmlUtil.getAttr(tag, "docBase");
			String projectRoot=docBase.substring(0,docBase.indexOf("src/main/webapp/"));
			BuildUtil.buildSite(projectRoot);
		}
	}







}
