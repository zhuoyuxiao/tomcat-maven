package com.zyx.tomcat.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;



public class BuildUtil {
	
	public static void clearDirectory(String file) throws IOException {
		File fileObj = new File(file);
		if(fileObj.exists()){
			FileUtils.cleanDirectory(fileObj);
		}
	}
	public static void buildSite(String projectRoot) throws Exception {
		complie(projectRoot);
		cleanAndCopyFile(getTarget(projectRoot,"classes"),projectRoot+"/src/main/webapp/WEB-INF/classes");
		cleanAndCopyFile(getTarget(projectRoot,"lib"),projectRoot+"/src/main/webapp/WEB-INF/lib");
	}

	private static String getTarget(String projectRoot,String targetChildDir) {
		projectRoot=projectRoot.replace('\\', '/');
		for (File c : new File(projectRoot+"/target").listFiles()) {
			if(new File(c,"WEB-INF").exists()){
				String targetFile = c.getAbsolutePath()+"/WEB-INF/"+targetChildDir;
				if(!new File(targetFile).exists()){
					throw new RuntimeException("没有找到dir:"+targetFile);
				}
				return targetFile;
			}
		}
		throw new RuntimeException("没有找到package");
	}

	private static void cleanAndCopyFile(String fromFilePath,String destFilePath) throws IOException {
		clearDirectory(destFilePath);
		org.apache.commons.io.FileUtils.copyDirectory(new File(fromFilePath), new File(destFilePath));
	}

	private static void complie(String projectRoot) throws Exception {
		MyTomcatUtil.runCmds("cd /D "+projectRoot,"call mvn clean package -U -DskipTests");
	}


}
