package com.zyx.tomcat.util;


import javax.el.ArrayELResolver;
import javax.el.BeanELResolver;
import javax.el.CompositeELResolver;
import javax.el.ELContext;
import javax.el.ELException;
import javax.el.ELResolver;
import javax.el.FunctionMapper;
import javax.el.ListELResolver;
import javax.el.MapELResolver;
import javax.el.PropertyNotFoundException;
import javax.el.VariableMapper;

import org.apache.el.lang.EvaluationContext;
import org.apache.el.lang.ExpressionBuilder;

/**
 * el取值:
 * 实现:jasper-el.jar,el-api.jar实际上这些jar包tomcat本身都会有不需要依赖第三方包
 * @author huangyongsheng
 *
 */
public class TomcatELUtil {

	public static Object eval(Object context,String elExpression) {
		try {
			EvaluationContext ctx = new EvaluationContext(ELContextInnerImp.getInstance(context), null, null);
			return ExpressionBuilder.createNode(elExpression).getValue(ctx);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private final static class ELContextInnerImp extends ELContext {
		private static ELResolverImp resolver;
		private Object base;
		private static class ELResolverImp extends CompositeELResolver {
			public Object getValue(ELContext context, Object base, Object property) throws NullPointerException, PropertyNotFoundException, ELException {
				if (base == null) {
					ELContextInnerImp myContext = (ELContextInnerImp) context.getContext(ELContextInnerImp.class);
					base = myContext.base;
				}
				return super.getValue(context, base, property);
			}
		}
		public static ELContextInnerImp getInstance(Object base) {
			if (resolver == null) {
				synchronized (ELContextInnerImp.class) {
					if (resolver == null) {
						resolver = new ELResolverImp();
						resolver.add(new MapELResolver());
						resolver.add(new ListELResolver());
						resolver.add(new ArrayELResolver());
						resolver.add(new BeanELResolver());
					}
				}
			}
			ELContextInnerImp instance = new ELContextInnerImp();
			instance.base = base;
			instance.putContext(ELContextInnerImp.class, instance);
			return instance;
		}

		@Override
		public ELResolver getELResolver() {
			return resolver;
		}
		public FunctionMapper getFunctionMapper() {
			return null;
		}
		@Override
		public VariableMapper getVariableMapper() {
			return null;
		}

	}


}
