package com.zyx.tomcat.util;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Set;

public class FileUtil {


    public static void copyAllFile(File source, File targetDir, Set<String> whiteList) throws IOException {
        if(source.isDirectory()) {
            for (File sourceItem : source.listFiles()) {
                copyAllFile(sourceItem, targetDir, whiteList);
            }
        }else {
            if(whiteList==null || whiteList.contains(source.getName())) {
                FileUtils.copyFileToDirectory(source, targetDir);
            }
        }
    }
    public static void copyDirectory(String srcDir, String destDir) throws IOException {
        FileUtils.copyDirectory(new File(srcDir), new File(destDir));
    }
    public static void delete(String dir) {
        new File(dir).delete();
    }
	public static String readToString(String absolutePath) throws IOException {
		return FileUtils.readFileToString(new File(absolutePath));
	}
	
	public static void writeFile(String absolutePath, String content) throws IOException {
		FileUtils.write(new File(absolutePath), content);
	}
    
}
