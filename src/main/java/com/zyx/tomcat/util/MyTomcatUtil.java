package com.zyx.tomcat.util;

import java.io.File;

import org.apache.commons.io.FileUtils;



public class MyTomcatUtil {
	public static final String LineSeparator = System.getProperty("line.separator");

	
	public static void runCmds(String...cmds) throws Exception {
		StringBuilder sb=new StringBuilder("").append(LineSeparator);
		for (String cmd : cmds) {
			sb.append(cmd).append(LineSeparator);
		}
//		sb.append("pause").append(LineSeparator);
		sb.append("if %ERRORLEVEL% NEQ 0 pause").append(LineSeparator);
		sb.append("exit").append(LineSeparator);
		String file = getTmpDirWithTs("tomcat_tmp.bat");
		FileUtils.writeStringToFile(new File(file), sb.toString(),"GBK");
		runBat(file);
	}
	private static void runBat(String bat) throws Exception {
		run(String.format("cmd.exe /c start %s", bat),true);
	}

	private static void run(String cmd, boolean isWaitFor) throws Exception {
		Process p = Runtime.getRuntime().exec(cmd);
		if (isWaitFor) {
			p.getInputStream().read();
		}
	}
	
	
	private static String getTmpDirWithTs(String fileNameSuffix) {
		return getTmpDir(System.currentTimeMillis()+"_"+fileNameSuffix);
	}


	private static String getTmpDir(String fileName) {
		String tmpDir = "e:/cmd_tmp";
		File tmpDirFile = new File(tmpDir);
		tmpDirFile.mkdirs();
		return tmpDir+"/"+fileName;
	}

}
